module Hazard_detection(
    ID_EX_MemRead_i,
    ID_EX_Rt_i,
	IF_ID_Rs_i,
    IF_ID_Rt_i,
    PC_Src_i, //branch or not
    PC_Write_o,
    IF_ID_Write_o,
	IF_Flush_o,
    ID_Flush_o,
    EX_Flush_o
);
input ID_EX_MemRead_i;
input [5-1:0] ID_EX_Rt_i, IF_ID_Rs_i, IF_ID_Rt_i;
input PC_Src_i;

output reg PC_Write_o;
output reg IF_ID_Write_o;
output reg IF_Flush_o;
output reg ID_Flush_o;
output reg EX_Flush_o;

always@(*)begin
    if(PC_Src_i)begin //branch detected
        PC_Write_o=1;
        IF_ID_Write_o=1;
        ID_Flush_o=1; //clear to 0
        IF_Flush_o=1;
        EX_Flush_o=1;
    end
    else if(ID_EX_MemRead_i && (ID_EX_Rt_i==IF_ID_Rs_i || ID_EX_Rt_i==IF_ID_Rt_i))begin
        PC_Write_o=0;
        IF_ID_Write_o=0;
        ID_Flush_o=0;
        IF_Flush_o=0;
        EX_Flush_o=0;
    end else
    begin
        PC_Write_o=1;
        IF_ID_Write_o=1;
        ID_Flush_o=0;
        IF_Flush_o=0;
        EX_Flush_o=0;
    end
end
endmodule

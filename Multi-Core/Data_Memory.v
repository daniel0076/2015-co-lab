`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    17:31:32 08/18/2010
// Design Name:
// Module Name:    Data_Memory
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module Data_Memory
(
    clk_i,
    addr0_i,
    data0_i,
    MemRead0_i,
    MemWrite0_i,
    data0_o,

    addr1_i,
    data1_i,
    MemRead1_i,
    MemWrite1_i,
    data1_o
);

// Interface
input               clk_i;
input   [31:0]      addr0_i;
input   [31:0]      data0_i;
input               MemRead0_i;
input               MemWrite0_i;
output  [31:0]      data0_o;

input   [31:0]      addr1_i;
input   [31:0]      data1_i;
input               MemRead1_i;
input               MemWrite1_i;
output  [31:0]      data1_o;

// Signals
reg     [31:0]      data0_o;
reg     [31:0]      data1_o;

// Memory
reg  signed     [7:0]    	Mem        		[0:127];    // address: 0x00~0x80
integer             i;

// For Testbench to debug
wire signed   [31:0]      memory          [0:31];
assign  memory[0] = {Mem[3], Mem[2], Mem[1], Mem[0]};
assign  memory[1] = {Mem[7], Mem[6], Mem[5], Mem[4]};
assign  memory[2] = {Mem[11], Mem[10], Mem[9], Mem[8]};
assign  memory[3] = {Mem[15], Mem[14], Mem[13], Mem[12]};
assign  memory[4] = {Mem[19], Mem[18], Mem[17], Mem[16]};
assign  memory[5] = {Mem[23], Mem[22], Mem[21], Mem[20]};
assign  memory[6] = {Mem[27], Mem[26], Mem[25], Mem[24]};
assign  memory[7] = {Mem[31], Mem[30], Mem[29], Mem[28]};
assign  memory[8] = {Mem[35], Mem[34], Mem[33], Mem[32]};
assign  memory[9] = {Mem[39], Mem[38], Mem[37], Mem[36]};
assign  memory[10] = {Mem[43], Mem[42], Mem[41], Mem[40]};
assign  memory[11] = {Mem[47], Mem[46], Mem[45], Mem[44]};
assign  memory[12] = {Mem[51], Mem[50], Mem[49], Mem[48]};
assign  memory[13] = {Mem[55], Mem[54], Mem[53], Mem[52]};
assign  memory[14] = {Mem[59], Mem[58], Mem[57], Mem[56]};
assign  memory[15] = {Mem[63], Mem[62], Mem[61], Mem[60]};
assign  memory[16] = {Mem[67], Mem[66], Mem[65], Mem[64]};
assign  memory[17] = {Mem[71], Mem[70], Mem[69], Mem[68]};
assign  memory[18] = {Mem[75], Mem[74], Mem[73], Mem[72]};
assign  memory[19] = {Mem[79], Mem[78], Mem[77], Mem[76]};
assign  memory[20] = {Mem[83], Mem[82], Mem[81], Mem[80]};
assign  memory[21] = {Mem[87], Mem[86], Mem[85], Mem[84]};
assign  memory[22] = {Mem[91], Mem[90], Mem[89], Mem[88]};
assign  memory[23] = {Mem[95], Mem[94], Mem[93], Mem[92]};
assign  memory[24] = {Mem[99], Mem[98], Mem[97], Mem[96]};
assign  memory[25] = {Mem[103], Mem[102], Mem[101], Mem[100]};
assign  memory[26] = {Mem[107], Mem[106], Mem[105], Mem[104]};
assign  memory[27] = {Mem[111], Mem[110], Mem[109], Mem[108]};
assign  memory[28] = {Mem[115], Mem[114], Mem[113], Mem[112]};
assign  memory[29] = {Mem[119], Mem[118], Mem[117], Mem[116]};
assign  memory[30] = {Mem[123], Mem[122], Mem[121], Mem[120]};
assign  memory[31] = {Mem[127], Mem[126], Mem[125], Mem[124]};

initial begin
	for(i=0; i<128; i=i+1)
        Mem[i]=8'b0;
    Mem[0]=1;
    Mem[4]=3;
	{Mem[11], Mem[10], Mem[9], Mem[8]} = -100;
    Mem[12]=2;
    Mem[16]=2;
    Mem[20]=10;
    Mem[24]=3;
    Mem[28]=1;
    Mem[32]=0;
    Mem[36]=3;
    Mem[40]=4;
	//Mem[44]=-2;
	{Mem[47], Mem[46], Mem[45], Mem[44]} = -2;
    Mem[48]=5;
	//Mem[52]=-1;
    {Mem[55], Mem[54], Mem[53], Mem[52]} = -1;
    Mem[56]=6;

end

always@(posedge clk_i) begin
    if(MemWrite0_i) begin
        Mem[addr0_i+3] <= data0_i[31:24];
        Mem[addr0_i+2] <= data0_i[23:16];
        Mem[addr0_i+1] <= data0_i[15:8];
        Mem[addr0_i]   <= data0_i[7:0];
    end
    if(MemWrite1_i) begin
        Mem[addr1_i+3] <= data1_i[31:24];
        Mem[addr1_i+2] <= data1_i[23:16];
        Mem[addr1_i+1] <= data1_i[15:8];
        Mem[addr1_i]   <= data1_i[7:0];
    end
end

always@(addr0_i or MemRead0_i) begin
    if(MemRead0_i)
        data0_o = {Mem[addr0_i+3], Mem[addr0_i+2], Mem[addr0_i+1], Mem[addr0_i]};
end

always@(addr1_i or MemRead1_i) begin
    if(MemRead1_i)
        data1_o = {Mem[addr1_i+3], Mem[addr1_i+2], Mem[addr1_i+1], Mem[addr1_i]};
end

endmodule


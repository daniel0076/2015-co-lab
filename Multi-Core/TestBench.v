//Subject:     CO project 4 - Test Bench
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:
//----------------------------------------------
//Date:
//----------------------------------------------
//Description:
//--------------------------------------------------------------------------------
`timescale 1ns / 1ps
`define CYCLE_TIME 10

module TestBench;

//Internal Signals
reg         CLK;
reg         RST;
integer     count;
integer     i;
integer     handle;

//shared memory

wire [32-1:0] cpu_dm_addr0_i;
wire [32-1:0] cpu_dm_data0_w;
wire cpu_dm_MemRead0_i;
wire cpu_dm_MemWrite0_i;
wire [32-1:0] dm_cpu_data0_o;

wire [32-1:0] cpu_dm_addr1_i;
wire [32-1:0] cpu_dm_data1_w;
wire cpu_dm_MemRead1_i;
wire cpu_dm_MemWrite1_i;
wire [32-1:0] dm_cpu_data1_o;


//Share data memory
Data_Memory DM(
        .clk_i(CLK),
        .addr0_i(cpu_dm_addr0_i),
        .data0_i(cpu_dm_data0_w),
        .MemRead0_i(cpu_dm_MemRead0_i),
        .MemWrite0_i(cpu_dm_MemWrite0_i),
        .data0_o(dm_cpu_data0_o),

        .addr1_i(cpu_dm_addr1_i),
        .data1_i(cpu_dm_data1_w),
        .MemRead1_i(cpu_dm_MemRead1_i),
        .MemWrite1_i(cpu_dm_MemWrite1_i),
        .data1_o(dm_cpu_data1_o)
        );

//Greate tested modle
Pipe_CPU_1 cpu0(
        .clk_i(CLK),
	    .rst_i(RST),
        .Mem_addr_o(cpu_dm_addr0_i),
        .WriteData_o(cpu_dm_data0_w),
        .MemRead_o(cpu_dm_MemRead0_i),
        .MemWrite_o(cpu_dm_MemWrite0_i),
        .ReadData_i(dm_cpu_data0_o)
		);

Pipe_CPU_1 cpu1(
        .clk_i(CLK),
	    .rst_i(RST),
        .Mem_addr_o(cpu_dm_addr1_i),
        .WriteData_o(cpu_dm_data1_w),
        .MemRead_o(cpu_dm_MemRead1_i),
        .MemWrite_o(cpu_dm_MemWrite1_i),
        .ReadData_i(dm_cpu_data1_o)
		);

//Main function

always #(`CYCLE_TIME/2) CLK = ~CLK;

initial  begin
$readmemb("LAB6_machine_1.txt", cpu0.IM.instruction_file);
$readmemb("LAB6_machine_2.txt", cpu1.IM.instruction_file);

	CLK = 0;
	RST = 0;
	count = 0;

    #(`CYCLE_TIME)      RST = 1;
    #(`CYCLE_TIME*40)      $stop;

end


always@(posedge CLK) begin
    count = count + 1;
	if( count >0) begin
	//print result to transcript
	$display("\n=============================round %d================================",count);
	$display("Core1==Register===========================================================\n");
	$display("r0=%d, r1=%d, r2=%d, r3=%d, r4=%d, r5=%d, r6=%d, r7=%d\n",
	cpu0.RF.Reg_File[0], cpu0.RF.Reg_File[1], cpu0.RF.Reg_File[2], cpu0.RF.Reg_File[3], cpu0.RF.Reg_File[4],
	cpu0.RF.Reg_File[5], cpu0.RF.Reg_File[6], cpu0.RF.Reg_File[7],
	);
	$display("r8=%d, r9=%d, r10=%d, r11=%d, r12=%d, r13=%d, r14=%d, r15=%d\n",
	cpu0.RF.Reg_File[8], cpu0.RF.Reg_File[9], cpu0.RF.Reg_File[10], cpu0.RF.Reg_File[11], cpu0.RF.Reg_File[12],
	cpu0.RF.Reg_File[13], cpu0.RF.Reg_File[14], cpu0.RF.Reg_File[15],
	);
	$display("r16=%d, r17=%d, r18=%d, r19=%d, r20=%d, r21=%d, r22=%d, r23=%d\n",
	cpu0.RF.Reg_File[16], cpu0.RF.Reg_File[17], cpu0.RF.Reg_File[18], cpu0.RF.Reg_File[19], cpu0.RF.Reg_File[20],
	cpu0.RF.Reg_File[21], cpu0.RF.Reg_File[22], cpu0.RF.Reg_File[23],
	);
	$display("r24=%d, r25=%d, r26=%d, r27=%d, r28=%d, r29=%d, r30=%d, r31=%d\n",
	cpu0.RF.Reg_File[24], cpu0.RF.Reg_File[25], cpu0.RF.Reg_File[26], cpu0.RF.Reg_File[27], cpu0.RF.Reg_File[28],
	cpu0.RF.Reg_File[29], cpu0.RF.Reg_File[30], cpu0.RF.Reg_File[31],
	);

	$display("Core2==Register===========================================================\n");
	$display("r0=%d, r1=%d, r2=%d, r3=%d, r4=%d, r5=%d, r6=%d, r7=%d\n",
	cpu1.RF.Reg_File[0], cpu1.RF.Reg_File[1], cpu1.RF.Reg_File[2], cpu1.RF.Reg_File[3], cpu1.RF.Reg_File[4],
	cpu1.RF.Reg_File[5], cpu1.RF.Reg_File[6], cpu1.RF.Reg_File[7],
	);
	$display("r8=%d, r9=%d, r10=%d, r11=%d, r12=%d, r13=%d, r14=%d, r15=%d\n",
	cpu1.RF.Reg_File[8], cpu1.RF.Reg_File[9], cpu1.RF.Reg_File[10], cpu1.RF.Reg_File[11], cpu1.RF.Reg_File[12],
	cpu1.RF.Reg_File[13], cpu1.RF.Reg_File[14], cpu1.RF.Reg_File[15],
	);
	$display("r16=%d, r17=%d, r18=%d, r19=%d, r20=%d, r21=%d, r22=%d, r23=%d\n",
	cpu1.RF.Reg_File[16], cpu1.RF.Reg_File[17], cpu1.RF.Reg_File[18], cpu1.RF.Reg_File[19], cpu1.RF.Reg_File[20],
	cpu1.RF.Reg_File[21], cpu1.RF.Reg_File[22], cpu1.RF.Reg_File[23],
	);
	$display("r24=%d, r25=%d, r26=%d, r27=%d, r28=%d, r29=%d, r30=%d, r31=%d\n",
	cpu1.RF.Reg_File[24], cpu1.RF.Reg_File[25], cpu1.RF.Reg_File[26], cpu1.RF.Reg_File[27], cpu1.RF.Reg_File[28],
	cpu1.RF.Reg_File[29], cpu1.RF.Reg_File[30], cpu1.RF.Reg_File[31],
	);

	$display("\nMemory===========================================================\n");
	$display("m0=%d, m1=%d, m2=%d, m3=%d, m4=%d, m5=%d, m6=%d, m7=%d\n\nm8=%d, m9=%d, m10=%d, m11=%d, m12=%d, m13=%d, m14=%d, m15=%d\n\nr16=%d, m17=%d, m18=%d, m19=%d, m20=%d, m21=%d, m22=%d, m23=%d\n\nm24=%d, m25=%d, m26=%d, m27=%d, m28=%d, m29=%d, m30=%d, m31=%d",
	          DM.memory[0], DM.memory[1], DM.memory[2], DM.memory[3],
				 DM.memory[4], DM.memory[5], DM.memory[6], DM.memory[7],
				 DM.memory[8], DM.memory[9], DM.memory[10], DM.memory[11],
				 DM.memory[12], DM.memory[13], DM.memory[14], DM.memory[15],
				 DM.memory[16], DM.memory[17], DM.memory[18], DM.memory[19],
				 DM.memory[20], DM.memory[21], DM.memory[22], DM.memory[23],
				 DM.memory[24], DM.memory[25], DM.memory[26], DM.memory[27],
				 DM.memory[28], DM.memory[29], DM.memory[30], DM.memory[31]
			  );
	//$display("\nPC=%d\n",cpu.PC.pc_i);
	end
	else ;
end

endmodule


//Subject:     CO project 2 - ALU
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:
//----------------------------------------------
//Date:
//----------------------------------------------
//Description:
//--------------------------------------------------------------------------------

module ALU(
    src1_i,
    src2_i,
    ctrl_i,
    result_o,
    zero_o
    );

//I/O ports
input signed [32-1:0]  src1_i;
input signed [32-1:0]  src2_i;
input        [4-1:0]   ctrl_i;

output signed [32-1:0]  result_o;
output           zero_o;

//Internal signals
reg    [32-1:0]  result_o;
wire             zero_o;


//Parameter

//Main function
/*your code here*/
always@(*)begin
    case (ctrl_i)
        4'b0000:result_o=src1_i & src2_i; //AND
        4'b0001:result_o=src1_i | src2_i; //OR
        4'b0010:result_o=src1_i + src2_i; //ADD
        4'b0011:result_o=src1_i <<< src2_i; //SLL
        4'b0100:result_o=src1_i != src2_i; //BNE
        4'b0101:result_o=src1_i >>> src2_i; //SRL
        4'b0110:result_o=src1_i-src2_i; //SUB
        4'b0111:result_o=src1_i<src2_i;  //SLT
        4'b1000:result_o=(src1_i == src2_i); //BEQ than jump
        4'b1001:result_o=(src1_i * src2_i ); //MUL
        4'b1010:result_o=(src1_i > src2_i ); //BGT
        4'b1011:result_o=(src1_i >= 0); //BGEZ
        4'b1110:result_o=src2_i * (2<<15); //LUI



        default:result_o='hxxxx_xxxx;
    endcase
end
assign zero_o = !result_o;


endmodule


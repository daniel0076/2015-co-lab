//Subject:     CO project 2 - ALU Controller
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:
//----------------------------------------------
//Date:
//----------------------------------------------
//Description:
//--------------------------------------------------------------------------------

module ALU_Ctrl(
          funct_i,
          ALUOp_i,
          ALUCtrl_o
          );

//I/O ports
input      [6-1:0] funct_i; //Function Field
input      [4-1:0] ALUOp_i;

output     [4-1:0] ALUCtrl_o;

//Internal Signals
reg        [4-1:0] ALUCtrl_o;

//Parameter
parameter lw_sw_op = 4'b0000;
parameter branch_op = 4'b0001;
parameter arith_op = 4'b0010;
//I type
parameter addi_op = 4'b0011;
parameter slti_op = 4'b0100;
parameter ori_op = 4'b0101;
parameter bne_op = 4'b0110;
//custome SLL/SRLV/LUI
parameter lui_op = 4'b0111;
parameter bgt_op = 4'b1000;
parameter bgez_op = 4'b1001;

//ALUCTRL_o
parameter _and = 4'b0000;
parameter _or  = 4'b0001;
parameter _add = 4'b0010;
parameter _sll = 4'b0011;
parameter _bne = 4'b0100;
parameter _srl = 4'b0101;
parameter _sub = 4'b0110;
parameter _slt = 4'b0111;
parameter _beq= 4'b1000;
parameter _mul= 4'b1001;
parameter _bgt= 4'b1010;
parameter _bgez= 4'b1011;
parameter _lui = 4'b1110;


//Select exact operation
always@(*)begin
    case(ALUOp_i)
        lw_sw_op:begin //lw sw
            ALUCtrl_o=_add;
        end
        branch_op:begin //beq
            ALUCtrl_o=_beq;
        end
        bne_op:begin //bne
            ALUCtrl_o=_bne;
        end
        bgt_op:begin //bgt
            ALUCtrl_o=_bgt;
        end
        bgez_op:begin //bne
            ALUCtrl_o=_bgez;
        end
        arith_op:begin //arithmetic
            case(funct_i)
                6'b000000:ALUCtrl_o= _sll;
                6'b000110:ALUCtrl_o= _srl;
                6'b011000:ALUCtrl_o= _mul;
                6'b100000:ALUCtrl_o= _add;
                6'b100010:ALUCtrl_o= _sub;
                6'b100100:ALUCtrl_o= _and;
                6'b100101:ALUCtrl_o= _or;
                6'b101010:ALUCtrl_o= _slt;
            endcase
        end
        addi_op:begin //ADDI
            ALUCtrl_o = _add;
        end
        slti_op:begin //SLTI
            ALUCtrl_o = _slt;
        end
        ori_op:begin //ORI
            ALUCtrl_o = _or;
        end
        lui_op:begin //lui
            ALUCtrl_o = _lui;
        end
    endcase
end
endmodule









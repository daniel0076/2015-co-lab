//Subject:     CO project 2 - Decoder
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:      0216007
//----------------------------------------------
//Date:        2015/4/25
//----------------------------------------------
//Description:
//--------------------------------------------------------------------------------

module Decoder(
    instr_op_i,
    RegWrite_o,
    ALU_op_o,
    ALUSrc_o,
    RegDst_o,
    Branch_o,
    BranchType_o,
    Jump_o,
    MemRead_o,
    MemWrite_o,
    MemtoReg_o,
);

//I/O ports
input  [6-1:0] instr_op_i;

output         RegWrite_o;
output [4-1:0] ALU_op_o;
output         ALUSrc_o;
output         RegDst_o;
output         Branch_o;

output [2-1:0] BranchType_o;
output         Jump_o;
output         MemRead_o;
output         MemWrite_o;
output [2-1:0] MemtoReg_o;

//Internal Signals
reg    [4-1:0] ALU_op_o;
reg            ALUSrc_o;
reg            RegWrite_o;
reg            RegDst_o;
reg            Branch_o;

reg [2-1:0] BranchType_o;
reg         Jump_o;
reg         MemRead_o;
reg         MemWrite_o;
reg [2-1:0] MemtoReg_o;

//Parameter
parameter lw_sw_op = 4'b0000;
parameter branch_op = 4'b0001;
parameter arith_op = 4'b0010;
//I type
parameter addi_op = 4'b0011;
parameter slti_op = 4'b0100;
parameter ori_op = 4'b0101;
parameter bne_op = 4'b0110;
//custome SLL/SRLV/LUI
parameter lui_op = 4'b0111;
parameter bgt_op = 4'b1000;
parameter bgez_op = 4'b1001;

always@(*)begin
    case(instr_op_i)
        6'b100011:begin //lw
            ALU_op_o= lw_sw_op;
            ALUSrc_o = 1; //add from imme
            RegDst_o = 0; //write to rt
            Branch_o = 0;
            MemRead_o  = 1;
            MemWrite_o = 0;
            RegWrite_o = 1;
            Jump_o = 0;
            //BranchType_o //don't care
            MemtoReg_o = 2'b01; //From DM
        end
        6'b101011:begin //sw
            ALU_op_o = lw_sw_op;
            ALUSrc_o = 1; //add from imme
            RegDst_o = 0; //write to rt
            Branch_o = 0;
            MemRead_o  = 0;
            MemWrite_o = 1;
            RegWrite_o = 0;
            Jump_o = 0;
            //BranchType_o; //don't care
            //MemtoReg_o = 2'b01; //don't care
        end
        6'b000010:begin //jump
            Jump_o = 1;
            Branch_o = 0;
            MemRead_o  = 0;
            MemWrite_o = 0;
            RegWrite_o = 0;
        end
        6'b000000:begin //R-Type
            ALU_op_o = arith_op;
            ALUSrc_o = 0; //Load from register
            RegWrite_o = 1;
            RegDst_o = 1;//save to dst
            Branch_o = 0;
            MemRead_o  = 0;
            MemWrite_o = 0;
            Jump_o = 0;
            MemtoReg_o = 2'b00; //From ALU
        end

        6'b001000:begin //ADDI
            ALU_op_o = addi_op;
            ALUSrc_o = 1; //Load from immediate
            RegWrite_o = 1;
            RegDst_o = 0;
            Branch_o = 0;
            MemRead_o  = 0;
            MemWrite_o = 0;
            Jump_o = 0;
            MemtoReg_o = 2'b00; //From ALU
        end

        6'b001010:begin //SLTI
            ALU_op_o = slti_op;
            ALUSrc_o = 1; //Load from immediate
            RegWrite_o = 1;
            RegDst_o = 0;
            Branch_o = 0;
            MemRead_o  = 0;
            MemWrite_o = 0;
            Jump_o = 0;
            MemtoReg_o = 2'b00; //From ALU
        end

        6'b001101:begin //ORI
            ALU_op_o = ori_op;
            ALUSrc_o = 1; //Load from immediate
            RegWrite_o = 1;
            RegDst_o = 0;
            Branch_o = 0;
            MemRead_o  = 0;
            MemWrite_o = 0;
            Jump_o = 0;
            MemtoReg_o = 2'b00; //From ALU
        end

        6'b000100:begin //BEQ
            ALU_op_o = branch_op;
            ALUSrc_o = 0; //Load from register
            RegWrite_o = 0; //Branch don't write mem
            RegDst_o = 1'bx; //brench don't care this
            Branch_o = 1;
            BranchType_o= 2'b11;
            MemRead_o  = 0;
            MemWrite_o = 0;
            Jump_o = 0;
            MemtoReg_o = 2'b00; //From ALU
        end

        6'b000101:begin //BNE
            ALU_op_o = bne_op;
            ALUSrc_o = 0; //Load from register
            RegWrite_o = 0; //Branch don't write mem
            RegDst_o = 1'bx; //brench don't care this
            Branch_o = 1;
            BranchType_o= 2'b11;
            MemRead_o  = 0;
            MemWrite_o = 0;
            Jump_o = 0;
            MemtoReg_o = 2'b00; //From ALU
        end

        6'b00_1111:begin //LUI
            ALU_op_o = lui_op;
            ALUSrc_o = 1; //Load from instruction
            RegWrite_o = 1;
            RegDst_o = 0;
            Branch_o = 0;
            MemRead_o  = 0;
            MemWrite_o = 0;
            Jump_o = 0;
            MemtoReg_o = 2'b00; //From ALU
        end
        6'b000111:begin //bgt
            ALU_op_o = bgt_op;
            ALUSrc_o = 0; //Load from register
            RegWrite_o = 0; //Branch don't write mem
            RegDst_o = 1'bx; //brench don't care this
            Branch_o = 1;
            BranchType_o= 2'b11;
            MemRead_o  = 0;
            MemWrite_o = 0;
            Jump_o = 0;
            MemtoReg_o = 2'b00; //From ALU
        end
        6'b000001:begin //bgez
            ALU_op_o = bgez_op;
            ALUSrc_o = 1; //Load from immediate
            RegWrite_o = 0; //branch don't write
            RegDst_o = 1'bx;
            Branch_o = 1;
            BranchType_o= 2'b11;
            MemRead_o  = 0;
            MemWrite_o = 0;
            Jump_o = 0;
            MemtoReg_o = 2'b00; //From ALU
        end
    endcase
end


endmodule








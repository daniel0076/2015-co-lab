//Subject:     CO project 2 - Simple Single CPU
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer: 0216007
//----------------------------------------------
//Date:
//----------------------------------------------
//Description:
//--------------------------------------------------------------------------------
module Simple_Single_CPU(
        clk_i,
        rst_n
        );

//I/O port
input         clk_i;
input         rst_n;

//Internal Signles

wire [32-1:0] PC_o;
wire [32-1:0] IM_ins_o;
wire [32-1:0] Reg_ReadData1_o;
wire [32-1:0] Reg_ReadData2_o;
wire [32-1:0] Add_PC_4_o;
wire [32-1:0] Add_PC_Br_o;
wire [32-1:0] MUX_PC_SrcSelect_o;
wire [32-1:0] Shift_Imme_o;
wire [32-1:0] Extend_Imme_o;
wire [32-1:0] ALU_res_out;
wire [32-1:0] MUX_MemToReg_o;
wire [5-1:0]  MUX_Write_Reg_o;
wire [4-1:0] ALUCtrl_o;
wire [4-1:0] Dec_Ctrl_ALUOp_o;
wire [32-1:0] MUX_ALUSrc_data_o;
wire [32-1:0] MUX_BranchType_data_o;
wire [32-1:0] MUX_PC_Source_o;
wire [32-1:0] DM_Readdata_o;
wire [32-1:0] Jump_address;
wire [32-1:0] Shift_Jump_o;
wire [32-1:0] BranchType_Not_zero;
wire [32-1:0] BranchType_zero;
wire [32-1:0] BranchType_Not_res;
wire [32-1:0] BranchType_NAND_zero_res;
wire [2-1:0] Dec_Ctrl_MemToReg_o;
wire [2-1:0] Dec_Ctrl_BranchType_o;
wire Dec_Ctrl_Reg_Dst_o;
wire Dec_Ctrl_Branch_o;
wire Dec_Ctrl_RegWrite_o;
wire Dec_Ctrl_MemWrite_o;
wire Dec_Ctrl_MemRead_o;
wire Dec_Ctrl_ALUSrc_o;
wire Dec_Ctrl_Jump_o;
wire null_wire;
wire ALU_Zero_Flag_o;
wire Ctrl_BranchOrNot;



//Greate componentes
ProgramCounter PC(
        .clk_i(clk_i),
        .rst_i (rst_n),
        .pc_in_i(MUX_PC_SrcSelect_o) ,
        .pc_out_o(PC_o)
        );

Adder Adder1(
        .src1_i(PC_o),
        .src2_i(32'h0000_0004),
        .sum_o(Add_PC_4_o)
        );

Instr_Memory IM(
        .pc_addr_i(PC_o),
        .instr_o(IM_ins_o)
        );

Reg_File RF(
        .clk_i(clk_i),
        .rst_i(rst_n) ,
        .RSaddr_i(IM_ins_o[25:21]) ,
        .RTaddr_i(IM_ins_o[20:16]) ,
        .RDaddr_i(MUX_Write_Reg_o) ,
        .RDdata_i(MUX_MemToReg_o)  ,
        .RegWrite_i (Dec_Ctrl_RegWrite_o),
        .RSdata_o(Reg_ReadData1_o) ,
        .RTdata_o(Reg_ReadData2_o)
        );

MUX_2to1 #(.size(5)) Mux_Write_Reg(
        .data0_i(IM_ins_o[20:16]),
        .data1_i(IM_ins_o[15:11]),
        .select_i(Dec_Ctrl_Reg_Dst_o),
        .data_o(MUX_Write_Reg_o)
        );

MUX_4to1 #(.size(32)) Mux_MemToReg(
        .data0_i(ALU_res_out),
        .data1_i(DM_Readdata_o),
        .data2_i(Extend_Imme_o),
        .data3_i(null_wire),
        .select_i(Dec_Ctrl_MemToReg_o),
        .data_o(MUX_MemToReg_o)
        );

Data_Memory DM(
        .clk_i(clk_i),
        .addr_i(ALU_res_out),
        .data_i(Reg_ReadData2_o),
        .MemRead_i(Dec_Ctrl_MemRead_o),
        .MemWrite_i(Dec_Ctrl_MemWrite_o),
        .data_o(DM_Readdata_o)
        );

Decoder Decoder(
        .instr_op_i(IM_ins_o[31:26]),
        .RegWrite_o(Dec_Ctrl_RegWrite_o),
        .ALU_op_o(Dec_Ctrl_ALUOp_o),
        .ALUSrc_o(Dec_Ctrl_ALUSrc_o),
        .RegDst_o(Dec_Ctrl_Reg_Dst_o),
        .Branch_o(Dec_Ctrl_Branch_o),
        .BranchType_o(Dec_Ctrl_BranchType_o),
        .Jump_o(Dec_Ctrl_Jump_o),
        .MemRead_o(Dec_Ctrl_MemRead_o),
        .MemWrite_o(Dec_Ctrl_MemWrite_o),
        .MemtoReg_o(Dec_Ctrl_MemToReg_o)
        );

MUX_2to1 #(.size(32)) Mux_ALUSrc(
        .data0_i(Reg_ReadData2_o),
        .data1_i(Extend_Imme_o),
        .select_i(Dec_Ctrl_ALUSrc_o),
        .data_o(MUX_ALUSrc_data_o)
        );

MUX_4to1 #(.size(32)) Mux_Branch_Type(
        .data0_i(BranchType_zero),
        .data1_i(BranchType_NAND_zero_res),
        .data2_i(BranchType_Not_res),
        .data3_i(BranchType_Not_zero),
        .select_i(Dec_Ctrl_BranchType_o),
        .data_o(MUX_BranchType_data_o)
        );

Sign_Extend SE(
        .data_i(IM_ins_o[15:0]),
        .data_o(Extend_Imme_o)
        );

ALU_Ctrl AC(
        .funct_i(IM_ins_o[5:0]),
        .ALUOp_i(Dec_Ctrl_ALUOp_o),
        .ALUCtrl_o(ALUCtrl_o)
        );

ALU ALU(
        .src1_i(Reg_ReadData1_o),
        .src2_i(MUX_ALUSrc_data_o),
        .ctrl_i(ALUCtrl_o),
        .result_o(ALU_res_out),
        .zero_o(ALU_Zero_Flag_o)
        );

Adder Adder2(
        .src1_i(Add_PC_4_o),
        .src2_i(Shift_Imme_o),
        .sum_o(Add_PC_Br_o)
        );

Shift_Left_Two_32 PC_Shifter(
        .data_i(IM_ins_o[25:0]),
        .data_o(Shift_Jump_o)
        );

Shift_Left_Two_32 Imme_Shifter(
        .data_i(Extend_Imme_o),
        .data_o(Shift_Imme_o)
        );

MUX_2to1 #(.size(32)) Mux_PC_Jump(
        .data0_i(MUX_PC_Source_o),
        .data1_i(Jump_address),
        .select_i(Dec_Ctrl_Jump_o),
        .data_o(MUX_PC_SrcSelect_o)
        );

MUX_2to1 #(.size(32)) Mux_PC_Source(
        .data0_i(Add_PC_4_o),
        .data1_i(Add_PC_Br_o),
        .select_i(Ctrl_BranchOrNot),
        .data_o(MUX_PC_Source_o)
        );

assign Ctrl_BranchOrNot = Dec_Ctrl_Branch_o && MUX_BranchType_data_o;
assign Jump_address={ Add_PC_4_o[31:28],Shift_Jump_o[27:0]};
assign BranchType_zero=ALU_Zero_Flag_o;
assign BranchType_Not_zero=!ALU_Zero_Flag_o;
assign BranchType_NAND_zero_res=!(ALU_Zero_Flag_o && ALU_res_out);
assign BranchType_Not_res=!ALU_res_out;

endmodule




//Subject:     CO project 2 - ALU
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:
//----------------------------------------------
//Date:
//----------------------------------------------
//Description:
//--------------------------------------------------------------------------------

module ALU(
    src1_i,
	src2_i,
	ctrl_i,
	result_o,
	zero_o
	);

//I/O ports
input  [32-1:0] src1_i;
input  [32-1:0]	src2_i;
input  [4-1:0]  ctrl_i;

output [32-1:0]	result_o;
output           zero_o;

//Internal signals
reg	[32-1:0]  result_o;
wire	zero_o;

//Parameter

//Main function
wire signed	[31:0]	src1;
wire signed	[31:0]	src2;

assign src1 = src1_i;
assign src2 = src2_i;

always@(*)
begin
case(ctrl_i)
	4'b0000:	//AND
		result_o = src1 & src2;
	4'b0001:	//OR
		result_o = src1 | src2;
	4'b0010:	//ADD and ADDI
		result_o = src1 + src2;
	4'b0110:	//SUB
		result_o = src1 - src2;
	4'b0111:	//SLT and SLTI
		result_o = (src1 < src2);
	4'b1011:	//BEQ
		result_o = (src1 != src2);
	4'b1110:	//BNEZ
		result_o = (src1 == 0);
	4'b0011:	//SLL
		result_o = src1 <<< src2;
	4'b0100:	//SRLV
		result_o = src1 >>> src2;
	4'b0101:	//LUI
		result_o = src2 << 16;
	4'b1000:	//ORI
		result_o = src1 | src2;
	4'b1001:	//MUL
		result_o = src1 * src2;
	4'b1010:	//BGT
		result_o = (src1 <= src2);
	4'b1111:	//BGEZ
		result_o = (src1 < 0);
	default:
		result_o = 32'hxxxx_xxxx;
endcase
end

assign 	zero_o = (result_o == 32'h00000000)? 1'b1 : 1'b0;

endmodule

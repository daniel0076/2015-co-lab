//Subject:     CO project 2 - Decoder
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:      Luke
//----------------------------------------------
//Date:        2010/8/16
//----------------------------------------------
//Description:
//--------------------------------------------------------------------------------

module Decoder(
    instr_op_i,
    MemToReg_o,
    MemRead_o,
    MemWrite_o,
    RegWrite_o,
    ALU_op_o,
    ALUSrc_o,
    RegDst_o,
    Branch_o
    );

//I/O ports
input  [6-1:0] instr_op_i;

output         MemToReg_o;
output         MemRead_o;
output         MemWrite_o;
output         RegWrite_o;
output [4-1:0] ALU_op_o;
output         ALUSrc_o;
output         RegDst_o;
output         Branch_o;

//Internal Signals
reg            MemToReg_o;
reg            MemRead_o;
reg            MemWrite_o;
reg    [4-1:0] ALU_op_o;
reg            ALUSrc_o;
reg            RegWrite_o;
reg            RegDst_o;
reg            Branch_o;

//Parameter

//Main function

always@(*)
case(instr_op_i)
    6'b000000://R-type
        begin
        MemToReg_o = 1'b0;
        MemRead_o = 1'b0;
        MemWrite_o = 1'b0;
        RegDst_o = 1'b1;
        ALUSrc_o = 1'b0;
        RegWrite_o = 1'b1;
        Branch_o = 1'b0;
        ALU_op_o = 4'b0010;
        end
    6'b001000://I-type ADDI
        begin
        MemToReg_o = 1'b0;
        MemRead_o = 1'b0;
        MemWrite_o = 1'b0;
        RegDst_o = 1'b0;
        ALUSrc_o = 1'b1;
        RegWrite_o = 1'b1;
        Branch_o = 1'b0;
        ALU_op_o = 4'b0011;
        end
    6'b001010://I-type SLTI
        begin
        MemToReg_o = 1'b0;
        MemRead_o = 1'b0;
        MemWrite_o = 1'b0;
        RegDst_o = 1'b0;
        ALUSrc_o = 1'b1;
        RegWrite_o = 1'b1;
        Branch_o = 1'b0;
        ALU_op_o = 4'b0100;
        end
    6'b000100://BEQ
        begin
        MemToReg_o = 1'b0;
        MemRead_o = 1'b0;
        MemWrite_o = 1'b0;
        RegDst_o = 1'bx;
        ALUSrc_o = 1'b0;
        RegWrite_o = 1'b0;
        Branch_o = 1'b1;
        ALU_op_o = 4'b0001;
        end
    6'b000101://BNE or BNEZ
        begin
        MemToReg_o = 1'b0;
        MemRead_o = 1'b0;
        MemWrite_o = 1'b0;
        RegDst_o = 1'bx;
        ALUSrc_o = 1'b0;
        RegWrite_o = 1'b0;
        Branch_o = 1'b1;
        ALU_op_o = 4'b0101;
        end
    6'b001111://I-type LUI
        begin
        MemToReg_o = 1'b0;
        MemRead_o = 1'b0;
        MemWrite_o = 1'b0;
        RegDst_o = 1'b0;
        ALUSrc_o = 1'b1;
        RegWrite_o = 1'b1;
        Branch_o = 1'b0;
        ALU_op_o = 4'b0110;
        end
    6'b001101://I-type ORI
        begin
        MemToReg_o = 1'b0;
        MemRead_o = 1'b0;
        MemWrite_o = 1'b0;
        RegDst_o = 1'b0;
        ALUSrc_o = 1'b1;
        RegWrite_o = 1'b1;
        Branch_o = 1'b0;
        ALU_op_o = 4'b0111;
        end
    6'b100011://LW
        begin
        MemToReg_o = 1'b1;
        MemRead_o = 1'b1;
        MemWrite_o = 1'b0;
        RegDst_o = 1'b0;
        ALUSrc_o = 1'b1;
        RegWrite_o = 1'b1;
        Branch_o = 1'b0;
        ALU_op_o = 4'b0011;//same as addi
        end
    6'b101011://SW
        begin
        MemToReg_o = 1'bx;
        MemRead_o = 1'b0;
        MemWrite_o = 1'b1;
        RegDst_o = 1'b0;
        ALUSrc_o = 1'b1;
        RegWrite_o = 1'b0;
        Branch_o = 1'b0;
        ALU_op_o = 4'b0011;//same as addi
        end
    6'b000010://JUMP
        begin
        MemToReg_o = 1'bx;
        MemRead_o = 1'b0;
        MemWrite_o = 1'b0;
        RegDst_o = 1'b0;
        ALUSrc_o = 1'bx;
        RegWrite_o = 1'b0;
        Branch_o = 1'b0;
        ALU_op_o = 4'bxxxx;
        end
    6'b000111://BGT
        begin
        MemToReg_o = 1'b0;
        MemRead_o = 1'b0;
        MemWrite_o = 1'b0;
        RegDst_o = 1'b0;
        ALUSrc_o = 1'b0;
        RegWrite_o = 1'b0;
        Branch_o = 1'b1;
        ALU_op_o = 4'b1000;
        end
    6'b000001://BGEZ
        begin
        MemToReg_o = 1'b0;
        MemRead_o = 1'b0;
        MemWrite_o = 1'b0;
        RegDst_o = 1'b0;
        ALUSrc_o = 1'b1;
        RegWrite_o = 1'b0;
        Branch_o = 1'b1;
        ALU_op_o = 4'b1001;
        end
    default:
        begin
        MemToReg_o = 1'b0;
        MemRead_o = 1'b0;
        MemWrite_o = 1'b0;
        RegDst_o = 1'b0;
        ALUSrc_o = 1'b0;
        RegWrite_o = 1'b0;
        Branch_o = 1'b0;
        ALU_op_o = 4'b0000;
        end
endcase

endmodule

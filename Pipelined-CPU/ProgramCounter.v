//Subject:     CO project 2 - PC
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:      Luke
//----------------------------------------------
//Date:        2010/8/16
//----------------------------------------------
//Description:
//--------------------------------------------------------------------------------

module ProgramCounter(
    clk_i,
	rst_i,
	pc_in_i,
    pc_write_i,
	pc_out_o
	);

//I/O ports
input           clk_i;
input	        rst_i;
input           pc_write_i;
input  [32-1:0] pc_in_i;
output [32-1:0] pc_out_o;

//Internal Signals
reg    [32-1:0] pc_out_o;
reg    [32-1:0] pc_lock;

//Parameter


//Main function
always @(posedge clk_i) begin
    if(~rst_i)begin
        pc_out_o <= 0;
        pc_lock <=0;
    end
    else begin
        if(pc_write_i)begin
            pc_lock <=pc_in_i;
            pc_out_o <= pc_in_i;
        end else begin
            pc_lock<=pc_lock;
            pc_out_o <= pc_lock;
        end
    end
end

endmodule






//Subject:     CO project 2 - ALU Controller
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:      
//----------------------------------------------
//Date:        
//----------------------------------------------
//Description: 
//--------------------------------------------------------------------------------

module ALU_Ctrl(
          funct_i,
          ALUOp_i,
          ALUCtrl_o
          );
          
//I/O ports 
input      [6-1:0] funct_i;
input      [4-1:0] ALUOp_i;

output     [4-1:0] ALUCtrl_o;    
     
//Internal Signals
reg        [4-1:0] ALUCtrl_o;

//Parameter

//Select exact operation
always@(*)
case(ALUOp_i)
	4'b0001://BEQ
		ALUCtrl_o = 4'b1011;
	4'b0101://BNE
		ALUCtrl_o = 4'b1110;	
	4'b0010://Arithmetic
		case(funct_i)
			6'b100000://ADD
				ALUCtrl_o = 4'b0010;
			6'b100010://SUB
				ALUCtrl_o = 4'b0110;
			6'b100100://AND
				ALUCtrl_o = 4'b0000;
			6'b100101://OR
				ALUCtrl_o = 4'b0001;
			6'b101010://SLT
				ALUCtrl_o = 4'b0111;
			6'b000000://SLL
				ALUCtrl_o = 4'b0011;
			6'b000110://SRLV
				ALUCtrl_o = 4'b0100;
			6'b011000://MUL
				ALUCtrl_o = 4'b1001;
			default:
				ALUCtrl_o = 4'bxxxx;
		endcase
	4'b0011://ADDI
		ALUCtrl_o = 4'b0010;
	4'b0100://SLTI
		ALUCtrl_o = 4'b0111;
	4'b0110://LUI
		ALUCtrl_o = 4'b0101;
	4'b0111://ORI
		ALUCtrl_o = 4'b1000;
	4'b1000://BGT
		ALUCtrl_o = 4'b1010;
	4'b1001://BGEZ
		ALUCtrl_o = 4'b1111;
	default:
		ALUCtrl_o = 4'bxxxx;
endcase
endmodule
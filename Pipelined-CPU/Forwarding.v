
module Forwarding(
    EX_MEM_RegWrite_i,
    MEM_WB_RegWrite_i,
    EX_MEM_RegRd_i,
    MEM_WB_RegRd_i,
    ID_EX_RegRs_i,
    ID_EX_RegRt_i,
    ForwardA_o,
    ForwardB_o
);

//I/O ports
input EX_MEM_RegWrite_i;
input MEM_WB_RegWrite_i;
input [5-1:0] EX_MEM_RegRd_i;
input [5-1:0] MEM_WB_RegRd_i;
input [5-1:0] ID_EX_RegRs_i;
input [5-1:0] ID_EX_RegRt_i;
output reg [2-1:0] ForwardA_o;
output reg [2-1:0] ForwardB_o;

always@(*)begin
    //EX/MEM hazard
    if( (EX_MEM_RegWrite_i && EX_MEM_RegRd_i!=0) && EX_MEM_RegRd_i==ID_EX_RegRs_i )
        ForwardA_o = 2'b01;
    //MEM/WB hazard
    else if( (MEM_WB_RegWrite_i && MEM_WB_RegRd_i!=0) && MEM_WB_RegRd_i==ID_EX_RegRs_i )
        ForwardA_o = 2'b10;
    //normal
    else
        ForwardA_o=2'b00;


    //EX/MEM hazard
    if( (EX_MEM_RegWrite_i && EX_MEM_RegRd_i!=0) && EX_MEM_RegRd_i==ID_EX_RegRt_i )
        ForwardB_o = 2'b01;
    //MEM/WB hazard
    else if( (MEM_WB_RegWrite_i && MEM_WB_RegRd_i!=0) && MEM_WB_RegRd_i==ID_EX_RegRt_i )
        ForwardB_o = 2'b10;
    //normal
    else
        ForwardB_o = 2'b00;
end

endmodule

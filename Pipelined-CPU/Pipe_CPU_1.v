//Subject:     CO project 4 - Pipe CPU 1
//--------------------------------------------------------------------------------
//Version:     1
//--------------------------------------------------------------------------------
//Writer:
//----------------------------------------------
//Date:
//----------------------------------------------
//Description:
//--------------------------------------------------------------------------------
module Pipe_CPU_1(
        clk_i,
        rst_i
        );

/****************************************
I/O ports
****************************************/
input clk_i;
input rst_i;

/****************************************
Internal signal
****************************************/

/**** IF stage ****/
wire [32-1:0] pc_add_four_if;
wire [32-1:0] pc_in_i, pc_out_o;
wire [32-1:0] instr_if;
wire PCSrc_ctrl;

/**** ID stage ****/
wire [32-1:0] pc_add_four_id;
wire [32-1:0] instr_id, signed_extended_id;
wire [32-1:0] ReadData1_id, ReadData2_id;
//control signal
wire [4-1:0] ALU_op_id;
wire RegWrite_id, ALUSrc_id, RegDst_id, Branch_id, MemRead_id, MemWrite_id, MemToReg_id;
//Hazard detection
wire PC_Write_ctrl;
wire IF_ID_Write_ctrl;
wire ID_Flush_ctrl;
wire IF_Flush_ctrl;
wire EX_Flush_ctrl;
wire [11-1:0] ID_Flush_o;

/**** EX stage ****/
wire [32-1:0] pc_add_four_ex;
wire [32-1:0] signed_extended_ex;
wire [5-1:0] EX_Rs, EX_Rt, EX_Rd;
wire [32-1:0] ReadData1_ex, ReadData2_ex;
wire [32-1:0] ALU_input2, ALU_result_ex;
wire [32-1:0] Shift_left_ex;
wire [32-1:0] branch_result_ex;
wire [5-1:0] WriteRegister_ex;
wire zero_ex;
//control signal
wire [4-1:0] ALUCtrl_ex;
wire [4-1:0] ALU_op_ex;
wire RegWrite_ex, ALUSrc_ex, RegDst_ex, Branch_ex, MemRead_ex, MemWrite_ex, MemToReg_ex;
//forward
wire [2-1:0] ForwardA_o;
wire [2-1:0] ForwardB_o;
wire [32-1:0] Forward_ALU_Src1_Mux;
wire [32-1:0] Forward_ALU_Src2_Mux;
//hazard_detection
wire [2-1:0] EX_Flush_wb_o;
wire [3-1:0] EX_Flush_mem_o;

/**** MEM stage ****/
wire [32-1:0] ALU_result_mem;
wire [32-1:0] ReadData2_mem, ReadData_mem;
wire [32-1:0] branch_result_mem;
wire [5-1:0] WriteRegister_mem;
wire zero_mem;
//control signal
wire  RegWrite_mem, RegDst_mem, Branch_mem, MemRead_mem, MemWrite_mem, MemToReg_mem;

/**** WB stage ****/
wire [32-1:0] ReadData_wb, ALU_result_wb;
wire [32-1:0] WriteData_wb;
wire [5-1:0] WriteRegister_wb;
//control signal
wire  RegWrite_wb, MemToReg_wb;

/****************************************
Instnatiate modules
****************************************/
//Instantiate the components in IF stage
MUX_2to1 #(.size(32)) PC_Mux(
            .data0_i(pc_add_four_if),
            .data1_i(branch_result_mem),
            .select_i(PCSrc_ctrl), //PCSRC
            .data_o(pc_in_i)
        );


ProgramCounter PC(
        .clk_i(clk_i),
        .rst_i (rst_i),
		.pc_write_i(PC_Write_ctrl),
        .pc_in_i(pc_in_i) ,
        .pc_out_o(pc_out_o)
        );

Instruction_Memory IM(
            .addr_i(pc_out_o),
            .instr_o(instr_if)
        );

Adder Add_pc(
            .src1_i(pc_out_o),
            .src2_i(32'h0000_0004),
            .sum_o(pc_add_four_if)
        );

//IF/ID pipeline ------------------------------------------------------------------
IF_ID_Pipe_Reg #(.size(32+32)) IF_ID(       //N is the total length of input/output
            .rst_i(rst_i),
            .clk_i(clk_i),
			.flush_i(IF_Flush_ctrl),
			.pipe_write(IF_ID_Write_ctrl),
            .data_i({pc_add_four_if,instr_if}),
            .data_o({pc_add_four_id,instr_id})
        );
//------------------------------------------------------------------------------------

//Instantiate the components in ID stage
Reg_File RF(
        .clk_i(clk_i),
        .rst_n(rst_i),
        .RSaddr_i(instr_id[25:21]) ,
        .RTaddr_i(instr_id[20:16]) ,
        .RDaddr_i(WriteRegister_wb) ,
        .RDdata_i(WriteData_wb) ,
        .RegWrite_i(RegWrite_wb),
        .RSdata_o(ReadData1_id) ,
        .RTdata_o(ReadData2_id)
        );

Decoder Control(                            //This part is not sure and the decoder is not completed
        .instr_op_i(instr_id[31:26]),
        .MemToReg_o(MemToReg_id),
        .MemRead_o(MemRead_id),
        .MemWrite_o(MemWrite_id),
        .RegWrite_o(RegWrite_id),
        .ALU_op_o(ALU_op_id),
        .ALUSrc_o(ALUSrc_id),
        .RegDst_o(RegDst_id),
        .Branch_o(Branch_id)
        );

Sign_Extend Sign_Extend(
        .data_i(instr_id[15:0]),
        .data_o(signed_extended_id)
        );

Hazard_detection Hazard_detection(
        .ID_EX_MemRead_i(MemRead_ex),
        .ID_EX_Rt_i(EX_Rt),
        .IF_ID_Rs_i(instr_id[25:21]),
		.IF_ID_Rt_i(instr_id[20:16]),
        .PC_Src_i(PCSrc_ctrl),
        .PC_Write_o(PC_Write_ctrl),
        .IF_ID_Write_o(IF_ID_Write_ctrl),
		.IF_Flush_o(IF_Flush_ctrl),
        .ID_Flush_o(ID_Flush_ctrl),
        .EX_Flush_o(EX_Flush_ctrl)
        );

MUX_2to1 #(.size(11)) ID_Flush(
        .data0_i({MemToReg_id,MemRead_id,MemWrite_id,RegWrite_id,ALU_op_id,ALUSrc_id,RegDst_id,Branch_id}),
        .data1_i(11'b0),
        .select_i(ID_Flush_ctrl),
        .data_o(ID_Flush_o)
        );

//ID/EX pipeline ------------------------------------------------------------------
Pipe_Reg #(.size(11+32+32+32+32+5+5+5)) ID_EX(
            .rst_i(rst_i),
            .clk_i(clk_i),
            .data_i({ID_Flush_o,pc_add_four_id,ReadData1_id,ReadData2_id,signed_extended_id,instr_id[25:21],instr_id[20:16],instr_id[15:11]}),
            .data_o({MemToReg_ex,MemRead_ex,MemWrite_ex,RegWrite_ex,ALU_op_ex,ALUSrc_ex,RegDst_ex,Branch_ex,pc_add_four_ex,ReadData1_ex,ReadData2_ex,signed_extended_ex,EX_Rs,EX_Rt,EX_Rd})
        );
//------------------------------------------------------------------------------------

//Instantiate the components in EX stage
MUX_2to1 #(.size(2)) EX_Flush_wb(
        .data0_i({RegWrite_ex,MemToReg_ex}),
        .data1_i(2'b00),
        .select_i(EX_Flush_ctrl),
        .data_o(EX_Flush_wb_o)
        );

MUX_2to1 #(.size(3)) EX_Flush_mem(
        .data0_i({MemRead_ex,MemWrite_ex,Branch_ex}),
        .data1_i(3'b000),
        .select_i(EX_Flush_ctrl),
        .data_o(EX_Flush_mem_o)
        );

Shift_Left_Two_32 S_L_T(
        .data_i(signed_extended_ex),
        .data_o(Shift_left_ex)
        );

Adder Add_Branch(
            .src1_i(pc_add_four_ex),
            .src2_i(Shift_left_ex),
            .sum_o(branch_result_ex)
        );

MUX_4to1 #(.size(32)) Forward_ALU_Src1(
        .data0_i(ReadData1_ex),
        .data1_i(ALU_result_mem),
        .data2_i(WriteData_wb),
        .select_i(ForwardA_o),
        .data_o(Forward_ALU_Src1_Mux)
        );

MUX_4to1 #(.size(32)) Forward_ALU_Src2(
        .data0_i(ReadData2_ex),
        .data1_i(ALU_result_mem),
        .data2_i(WriteData_wb),
        .select_i(ForwardB_o),
        .data_o(Forward_ALU_Src2_Mux)
);

MUX_2to1 #(.size(32)) ALU_Src2(
        .data0_i(Forward_ALU_Src2_Mux),
        .data1_i(signed_extended_ex),
        .select_i(ALUSrc_ex),
        .data_o(ALU_input2)
        );

ALU ALU(
        .src1_i(Forward_ALU_Src1_Mux),
        .src2_i(ALU_input2),
        .ctrl_i(ALUCtrl_ex),
        .result_o(ALU_result_ex),
        .zero_o(zero_ex)
        );

ALU_Ctrl ALU_Control(
        .funct_i(signed_extended_ex[5:0]),
        .ALUOp_i(ALU_op_ex),
        .ALUCtrl_o(ALUCtrl_ex)
        );

MUX_2to1 #(.size(5)) EX_MEM_Src(
        .data0_i(EX_Rt),
        .data1_i(EX_Rd),
        .select_i(RegDst_ex),
        .data_o(WriteRegister_ex)
        );

Forwarding Forwarding_unit(
        .EX_MEM_RegWrite_i(RegWrite_mem),
        .MEM_WB_RegWrite_i(RegWrite_wb),
        .EX_MEM_RegRd_i(WriteRegister_mem),
        .MEM_WB_RegRd_i(WriteRegister_wb),
        .ID_EX_RegRs_i(EX_Rs),
        .ID_EX_RegRt_i(EX_Rt),
        .ForwardA_o(ForwardA_o),
        .ForwardB_o(ForwardB_o)
        );

//EX/MEM pipeline ------------------------------------------------------------------
Pipe_Reg #(.size(5+32+1+32+32+5)) EX_MEM(
        .rst_i(rst_i),
        .clk_i(clk_i),
        .data_i({EX_Flush_wb_o,EX_Flush_mem_o,branch_result_ex,zero_ex,ALU_result_ex,Forward_ALU_Src2_Mux,WriteRegister_ex}),
        .data_o({RegWrite_mem,MemToReg_mem,MemRead_mem,MemWrite_mem,Branch_mem,branch_result_mem,zero_mem,ALU_result_mem,ReadData2_mem,WriteRegister_mem})
        );
//------------------------------------------------------------------------------------

//Instantiate the components in MEM stage
Data_Memory DM(
        .clk_i(clk_i),
        .addr_i(ALU_result_mem),
        .data_i(ReadData2_mem),
        .MemRead_i(MemRead_mem),
        .MemWrite_i(MemWrite_mem),
        .data_o(ReadData_mem)
        );

//MEM/WB pipeline ------------------------------------------------------------------
Pipe_Reg #(.size(2+32+32+5)) MEM_WB(
        .rst_i(rst_i),
        .clk_i(clk_i),
        .data_i({RegWrite_mem, MemToReg_mem, ReadData_mem, ALU_result_mem, WriteRegister_mem}),
        .data_o({RegWrite_wb, MemToReg_wb, ReadData_wb, ALU_result_wb, WriteRegister_wb})
        );
//------------------------------------------------------------------------------------

//Instantiate the components in WB stage
MUX_2to1 #(.size(32)) WriteData_Src(
        .data0_i(ALU_result_wb),
        .data1_i(ReadData_wb),
        .select_i(MemToReg_wb),
        .data_o(WriteData_wb)
        );

/****************************************
signal assignment
****************************************/
assign PCSrc_ctrl = Branch_mem&zero_mem;
endmodule

